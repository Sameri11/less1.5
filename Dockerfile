FROM debian:9

RUN apt update \
    && apt install -y wget libseccomp2 libdevmapper1.02.1 iptables procps \
    && wget https://download.docker.com/linux/debian/dists/stretch/pool/stable/amd64/docker-ce_19.03.8~3-0~debian-stretch_amd64.deb \
    && wget https://download.docker.com/linux/debian/dists/stretch/pool/stable/amd64/containerd.io_1.2.6-3_amd64.deb \
    && wget https://download.docker.com/linux/debian/dists/stretch/pool/stable/amd64/docker-ce-cli_19.03.8~3-0~debian-stretch_amd64.deb \
    && dpkg -i $(ls / | grep 'containerd.io') \
    && dpkg -i $(ls / | grep 'docker-ce-cli_') \
    && dpkg -i $(ls / | grep 'docker-ce_') \
    && ls / | grep '.deb' | xargs rm -rf

RUN wget -O /usr/local/bin/dind "https://raw.githubusercontent.com/docker/docker/37498f009d8bf25fbb6199e8ccd34bed84f2874b/hack/dind"; \
    chmod +x /usr/local/bin/dind \
    && wget -O /usr/local/bin/dockerd-entrypoint.sh "https://raw.githubusercontent.com/docker-library/docker/a73d96e731e2dd5d6822c99a9af4dcbfbbedb2be/19.03/dind/dockerd-entrypoint.sh"; \
    chmod +x /usr/local/bin/dockerd-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/dockerd-entrypoint.sh"]
CMD []